# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Also based on](https://github.com/conventional-changelog/standard-version/blob/master/CHANGELOG.md) so decending.

## [0.1.2] - 2022-12-11
### Added
- adds favicon
- adds b64 encoded email and better copy

## [0.1.1] - 2022-12-11
### Changed
- changes font awesome icons

## [0.1.0] - 2022-12-06
### Added
- bot - adds changelog
- adds versionupdater and cl
- adds js code for contact form to local email

- adds versionupdater and cl